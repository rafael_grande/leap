var output = document.getElementById('output');
    Leap.loop(function (frame) {
        output.innerHTML = 'Frame: ' + frame.id;
    });
var ball = document.getElementById('ball');
    
ball.setTransform = function(position, rotation) {

  ball.style.left = position[0];
  ball.style.top  = position[1];

  ball.style.transform = 'rotate(' + -rotation + 'rad)';

};

Leap.loop(function(frame) {

  frame.hands.forEach(function(hand, index) {
    ball.setTransform(hand.screenPosition(), hand.roll());
  });

}).use('screenPosition', {scale: 0.25});